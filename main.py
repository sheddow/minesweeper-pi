import sys
import random

import numpy as np


# Generates an n x n board with n bombs
# where True represents a bomb and False is no bomb,
# then finds the sum by going through the entire board.
# Has time complexity O(n^2), so is pretty slow for big n.
def simulate_one_round(n):
    l = [True]*n + [False]*(n**2 - n)
    random.shuffle(l)

    board = np.array(l).reshape((n, n))

    bomb_count = 0
    for row in range(n):
        for col in range(n):
            if board[row][col]:
                continue # bomb at (row, col), so don't count

            # count bombs around (row, col)
            for row_offset in range(-1, 2):
                for col_offset in range(-1, 2):
                    if row_offset == 0 and col_offset == 0:
                        continue

                    has_bomb = (0 <= row + row_offset < n and
                                0 <= col + col_offset < n and
                                board[row + row_offset][col + col_offset])

                    if has_bomb:
                        bomb_count += 1


    return bomb_count / n


# Generates n random bomb positions,
# then finds the sum by counting around each bomb.
# Time complexity O(n)
def faster_sim_one_round(n):
    # pick n bomb positions and reshape into 2-d
    # use set() for faster lookup
    bomb_indices = set((k // n, k % n) for k in random.sample(range(n*n), n))

    bomb_count = 0
    for row, col in bomb_indices:
        for row_offset in range(-1, 2):
            for col_offset in range(-1, 2):
                if row_offset == 0 and col_offset == 0:
                    continue

                x = row + row_offset
                y = col + col_offset
                # increment bomb_count for surrounding squares that doesn't have a bomb
                increment_square = (0 <= x < n and
                                    0 <= y < n and
                                    (x, y) not in bomb_indices)

                if increment_square:
                    bomb_count += 1

    return bomb_count / n


def simulate(n, rounds=100):
    return sum(simulate_one_round(n) for _ in range(rounds)) / rounds


def faster_simulate(n, rounds=100):
    return sum(faster_sim_one_round(n) for _ in range(rounds)) / rounds


if __name__ == '__main__':
    if len(sys.argv) == 3:
        n = int(sys.argv[1])
        rounds = int(sys.argv[2])
    elif len(sys.argv) == 2:
        n = int(sys.argv[1])
        rounds = 100
    else:
        sys.exit("Usage: {} <n> [<rounds>]".format(sys.argv[0]))

    print(faster_simulate(n, rounds))
