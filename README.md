# The Minesweeper-π Conjecture

Let `S_n` be the expected value of the sum of numbers on a `n` x `n` minesweeper board with `n` randomly placed mines. This repository explores the hypothesis that `S_n / n` approaches `2.5 * π ≈ 7.854` as n goes to infinity.

## Simulation
`main.py` contains a Monte-Carlo simulation of the problem. For example, to test n = 1000 with 100 rounds, run `python main.py 1000 100`, which should return about 7.98. Higher values of n yields values closer to 8, implying that the hypothesis is false. There is also a Rust version which is ~10 times faster than the Python version. Build with `cargo build --release` and run `./target/release/minesweeper_pi <n> <rounds>`.

## Analysis
Ignoring edges and overlaps, each bomb increments 8 squares around it. On an `n` x `n` board with `n` bombs, there are O(n) edge squares, and O(n) squares that overlap with other bombs, which is asympotically smaller than the total number of squares O(n²). Therefore the edge cases can be ignored for large values of n, and the limit should approach 8.
