use std::collections::HashSet;

use rand::thread_rng;
use rand::seq::index::sample;



fn simulate_one_round(n: usize) -> f32 {
    let mut rng = thread_rng();

    let bomb_indices: HashSet<_> = sample(&mut rng, n*n, n)
        .iter()
        .map(|k| (k / n, k % n))
        .collect();

    let mut bomb_count = 0;
    for &(row, col) in &bomb_indices {
        for row_offset in -1..=1 {
            for col_offset in -1..=1 {
                if row_offset == 0 && col_offset == 0 {
                    continue
                }
                let x = (row as isize + row_offset) as usize;
                let y = (col as isize + col_offset) as usize;

                let increment_square = (0..n).contains(&x) &&
                                       (0..n).contains(&y) &&
                                       !bomb_indices.contains(&(x, y));

                if increment_square {
                    bomb_count += 1;
                }
            }
        }
    }

    bomb_count as f32 / n as f32
}


fn simulate(n: usize, rounds: usize) -> f32 {
    (0..rounds).map(|_| simulate_one_round(n)).sum::<f32>() / (rounds as f32)
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut args = std::env::args();
    let n = args.nth(1).ok_or("Usage: ./minesweeper_pi <n> [<rounds>]")?.parse()?;
    let rounds = args.nth(0).map(|s| s.parse().unwrap()).unwrap_or(100);

    println!("{}", simulate(n, rounds));

    Ok(())
}
